#OpenCV Object/Face tracker

A package to detect objects via OpenCV and a Webcam or an OpenNI camera for the Polymechanon team.

##First launch the camera driver:

Simple webcam:

```
$ roslaunch pol_vision uvc_cam.launch device:=/dev/video0 # or device:=/dev/video1
```

OpenNI:

```
$ roslaunch pol_vision openni_node.launch
```

##Then launch the node:

```
$ roslaunch pol_vision thenode.launch
```

Available nodes:

- `common.py`
- `face_tracker2_depth.py`
- `good_features.py`
- `template_tracker.py`
- `face_tracker2.py`
- `fast_template.py`
- `lk_tracker.py`
